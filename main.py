#!/usr/bin/env python3

PKG_NAME = "Contacts"
import os, sys

filepath = os.path.abspath(__file__)
fullpath = os.path.dirname(filepath) + '/'

from gi.repository import Gtk, Gdk, Pango, GdkPixbuf
from base64 import b64decode
from PIL import Image
import io, array

from vcard import Vcf

# exec(open(fullpath + "mysettings.conf", encoding="UTF-8").read())
# sys.path.append(PATH_MYLIB)
# from debugly import *

from pprint import pprint, pformat

#   ____ _   _ ___
#  / ___| | | |_ _|
# | |  _| | | || |
# | |_| | |_| || |
#  \____|\___/|___|
#
class GUI(Gtk.Window):
    def __init__(self, parent=None):
        Gtk.Window.__init__(self, title=PKG_NAME)
        self.set_default_size(700, 500)
        self.parent = parent

        self.makeWidgets()
        self.connect('key_press_event', self.key_binds)
        self.connect('delete-event', Gtk.main_quit)

        self.contacts.treeview.grab_focus()
        self.show_all()


    def makeWidgets(self):
        self.layout = Gtk.Grid()
        self.add(self.layout)

        self.toolbar = self.makeWidgets_toolbar()
        self.layout.attach(self.toolbar, left=0, top=0, width=5, height=1)
        # self.layout.attach(self.makeWidgets_recipient(), 0, 1, 5, 1)

        hpaned = Gtk.HPaned()
        self.layout.attach(hpaned, 0, 2, 5, 2)
        hpaned.set_position(250)

        self.contacts = self.makeWidgets_contacts()
        hpaned.add1(self.contacts)

        self.notebook = Gtk.Notebook()
        hpaned.add2(self.notebook)

        self.notebook.append_page(self.makeWidgets_viewer(), Gtk.Label("nothing"))


    def makeWidgets_toolbar(self):
        bar = Gtk.Toolbar()
        ## Open Gloss
        bar.b_Open = Gtk.ToolButton(icon_name=Gtk.STOCK_OPEN)
        bar.add(bar.b_Open)
        # bar.b_Open.connect("clicked", lambda w: self._open_dir())
        ##
        #
        bar.add(Gtk.SeparatorToolItem())
        #
        #
        ##  Preference
        bar.b_Preference = Gtk.ToolButton(icon_name=Gtk.STOCK_PREFERENCES)
        bar.add(bar.b_Preference)
        # bar.b_Preference.connect("clicked", lambda w: self.preference())
        ##
        ## About
        bar.b_About = Gtk.ToolButton(icon_name=Gtk.STOCK_ABOUT)
        # bar.b_About.connect("clicked", self._about_dialog)
        bar.add(bar.b_About)
        return bar


    def makeWidgets_contacts(self):
        layout = Gtk.VBox()
        layout.b_add = Gtk.Button.new_with_label(" Add New Contacts")
        layout.pack_start(layout.b_add, False, False, 0)
        icon_add = Gtk.Image.new_from_icon_name('contact-new', Gtk.IconSize.BUTTON)
        layout.b_add.set_image(icon_add)

        layout.pack_start(self.makeWidgets_treeview(parent=layout), True, True, 0)
        layout.current_selection = (None, None)

        self.searchbar = Gtk.SearchBar()
        layout.pack_start(self.searchbar, expand=False, fill=True, padding=0)

        self.search_entry = Gtk.SearchEntry()
        self.searchbar.connect_entry(self.search_entry)
        self.searchbar.add(self.search_entry)

        self.search_entry.connect("key_release_event", self.search_entry_on_key_release)

        return layout


    def search_entry_on_key_release(self, widget, event):
        if Gdk.ModifierType.CONTROL_MASK & event.state:
            if event.keyval == ord('c'): self.search_entry.set_text('')

        self.contacts.filter_name.refilter()


    def makeWidgets_treeview(self, parent):
        scroll = Gtk.ScrolledWindow()
        scroll.set_hexpand(True)
        scroll.set_vexpand(True)

        parent.treemodel = Gtk.TreeStore(GdkPixbuf.Pixbuf, str)

        parent.filter_name = parent.treemodel.filter_new()
        parent.filter_name.set_visible_func(self.contact_filter)

        treemodelsort = Gtk.TreeModelSort(parent.filter_name)
        treemodelsort.set_sort_column_id(1, Gtk.SortType.ASCENDING)

        parent.treeview = Gtk.TreeView.new_with_model(treemodelsort)
        scroll.add(parent.treeview)

        parent.treeview.set_headers_visible(False)
        parent.treeview.set_rubber_banding(True)

        ### 1
        treeviewcolumn = Gtk.TreeViewColumn("Distribution")
        parent.treeview.append_column(treeviewcolumn)

        cellrendererpixbuf = Gtk.CellRendererPixbuf()
        treeviewcolumn.pack_start(cellrendererpixbuf, False)
        treeviewcolumn.add_attribute(cellrendererpixbuf, "pixbuf", 0)
        ###

        ### 2
        renderer_text = Gtk.CellRendererText()
        parent.treeview.append_column(Gtk.TreeViewColumn("w", renderer_text, text=1))
        ###

        treeselection = parent.treeview.get_selection()
        treeselection.set_mode(Gtk.SelectionMode.MULTIPLE)
        parent.select_signal = treeselection.connect("changed", self._on_row_changed)

        return scroll


    def contact_filter(self, model, iterator, data):
        return self.search_entry.get_text().lower() in model[iterator][1].lower()


    def _on_row_changed(self, treeselection):
        model, pathlist = treeselection.get_selected_rows()
        pre_path, pre_treeiter = self.contacts.current_selection
        for path in pathlist:
            if ':' in str(path): continue
            row = model[path]
            vcard = contacts[row[1]]

            self.viewer.textbuffer.set_text(row[1]+'\n\n')
            self.pretty_printer(vcard)
            break

            name = pformat(vcard.get('TEL'))

            real_path = model.convert_path_to_child_path(path)
            tree_iter = self.contacts.treemodel.get_iter(real_path)

            if pre_treeiter:
                child = self.contacts.treemodel.iter_children(pre_treeiter)
                if child: self.contacts.treemodel.remove(child)

            self.contacts.treemodel.append(tree_iter, [ None, name ])
            self.contacts.treeview.expand_row(path, False)

            self.contacts.current_selection = (real_path, tree_iter)
            break


    def process_photo(self, photo):
        key = list(photo.keys())[0]
        base64dump = ''.join(photo[key].split())
        PATH_save = "/tmp/vcard_profile"
        hexdump = b64decode(base64dump)

        # FIXME: don't write in file
        with open(PATH_save, 'wb') as f:
            f.write(hexdump)

        im = Image.open(io.BytesIO(hexdump)) #; im.show()
        width, height = im.size

        pp = GdkPixbuf.Pixbuf.new_from_file_at_size(PATH_save, width, height)
        # arr = array.array('B', im.tostring())
        # width, height = im.size
        # pp = GdkPixbuf.Pixbuf.new_from_data(arr, GdkPixbuf.Colorspace.RGB, True, 8, width, height, width * 4)

        end = self.viewer.textbuffer.get_end_iter()
        self.viewer.textbuffer.insert_pixbuf(end, pp)
        end = self.viewer.textbuffer.get_end_iter()
        self.viewer.textbuffer.insert(end, "\n")


    def pretty_printer(self, vcard):
        for k, v in vcard.items():
            if k == "PHOTO": self.process_photo(v); continue
            if k in ( "VERSION", "PHOTO" ): continue
            end = self.viewer.textbuffer.get_end_iter()
            self.viewer.textbuffer.insert(end, k+":\t\t\t "+str(v)+'\n')


    def makeWidgets_viewer(self):
        vpaned = Gtk.VPaned()
        vpaned.set_position(300)

        scroll = Gtk.ScrolledWindow()
        scroll.set_hexpand(True)
        scroll.set_vexpand(True)

        self.viewer = Gtk.TextView()
        self.viewer.set_editable(False)
        self.viewer.set_cursor_visible(False)
        self.viewer.set_wrap_mode(Gtk.WrapMode.WORD)
        self.viewer.set_left_margin(10)
        self.viewer.set_right_margin(20)

        self.viewer.textbuffer = self.viewer.get_buffer()
        scroll.add(self.viewer)
        vpaned.add1(scroll)

        self.compose = self.makeWidgets_composer()
        vpaned.add2(self.compose)

        return vpaned


    def makeWidgets_composer(self):
        # layout = Gtk.HBox()

        # self.composer = Gtk.Label("hello")
        # layout.pack_start(self.composer, expand=False, fill=True, padding=0)

        # self.b_send = Gtk.Button(label="Send")
        # layout.pack_start(self.b_send, expand=False, fill=True, padding=0)
        return Gtk.TextView()
        return layout


    def key_binds(self, widget, event):
        if Gdk.ModifierType.CONTROL_MASK & event.state:
            if chr(event.keyval) in "fs":
                state = not self.searchbar.get_search_mode()
                self.searchbar.set_search_mode(state)
                self.contacts.filter_name.refilter()
                print(state)


def root_binds(widget, event):
    # print(event.keyval)
    if event.keyval == 65307:
        Gtk.main_quit()


def main():
    root = GUI()
    return root


if __name__ == '__main__':
    FILE_vcf = fullpath + "00001.vcf" if len(sys.argv) == 1 else sys.argv[1]

    root = main()
    root.connect("key_release_event", root_binds)

    contacts = Vcf(FILE_vcf)
    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(fullpath + "img/portrait.png", 36, 36)
    for c in contacts:
        root.contacts.treemodel.append(None, (pixbuf, c))

    Gtk.main()
