#!/usr/bin/env python3

import re
from collections import OrderedDict

# edisfds = ["DISPLAY NAME", "EMAIL"]
# fonefds = ["VOICE", "FAX", "CELL", "PAGER"]
# namefds = ["SURNAME", "FIRST NAME", "MIDDLE NAME(S)", "TITLE", "SUFFIX"]
# addrfds = ["PO ADDRESS", "EXTENDED ADDRESS", "STREET", "LOCALITY", "REGION", "POSTAL CODE", "COUNTRY"]
# itemnam = [ 'N', 'ADR', 'PHOTO', 'FN', 'VERSION', 'TEL', 'EMAIL', 'NOTE']
# ktypes = { "DOM", "INTL", "POSTAL", "PARCEL", "HOME", "WORK",
#            "PREF" , "VOICE" , "FAX" , "MSG" , "CELL" , "PAGER",
#            "INTERNET", "GIF", "BMP" , "MET" , "PMB" , "DIB", "PICT" , "TIFF" ,
#            "PDF" , "PS" , "JPEG" , "QTIME", "MPEG" , "MPEG2" , "AVI", "WAVE" , "AIFF" , "PCM", "X509" , "PGP"
#          }
# locale = {'WORK', 'HOME'} # to separate values where appropriate


wspace = r"[ \t]*"
linepat = r"""
            # a line with a name, params, values
            (?:[^\s[\]=:.,]+[.])*     # the groups pattern

            # the name pattern
            (?P<name>[A-Z-]+)

            # paramlist, with ":" at the end
            (?P<pars>(;{0}[A-Z-]+({0}={0}[A-Z-]+)?)*):

            # values..or [^\n\r\f\v]? same thing
            (?P<vals>(\S+[ ]*)+)
""".format(wspace)

re_begin = re.compile("{0}BEGIN{0}:{0}VCARD".format(wspace), re.I)
re_fold  = re.compile("(?: )(\S+[ ]*)+")
re_line  = re.compile(linepat, re.I | re.X)
re_end   = re.compile("END{0}:{0}VCARD".format(wspace), re.I)
re_vcfsplit = lambda d: re.split(r"(?<!\\);", d)

class Vcf(OrderedDict):
    def __init__(self, path):
        OrderedDict.__init__(self)
        # keep track of number of unknowns present
        self.uncount = 0
        self.parse(path)


    def parse(self, path):
        incard = False
        vco = dict()
        tag, attribs, vals = None, None, str()
        for line in open(path).read().splitlines():
            if re_begin.match(line):
                if incard: raise VcfError("Parse error: BEGIN before END")
                incard = True
                continue

            if re_end.match(line):
                incard = False
                if not tag: continue
                self.addtags(vco, tag, attrib, vals)
                self.addcard(vco)
                vco = dict()
                tag, attribs, vals = None, None, str()
                continue

            normal = re_line.match(line)
            if normal:
                if tag: self.addtags(vco, tag, attrib, vals)
                tag = normal.group('name')
                vals = normal.group('vals') # dont split yet
                attrib = re_vcfsplit(normal.group('pars')) if normal.group('pars') else normal.group('pars')
                continue

            fold = re_fold.match(line)
            if fold: vals += fold.group()


    def addtags(self, vco, tag, attribs, vals):
        # print(tag, vals, attribs)
        clean = [ v for v in re_vcfsplit(vals) if v ]
        v = clean if len(clean) > 1 else clean[0]

        for a in attribs:
            if a.find("TYPE") > -1:
                if not vco.get(tag): vco[tag] = dict()
                a_type = a.split("=")[1]
                vco[tag][a_type] = v
                break
            # elif a.strip(): print(a)
        else:
            vco[tag] = v


    def addcard(self, card):
        if not card.get('VERSION'): # set default version
            card['VERSION'] = '2.1'

        for tag in ['FN', 'N', 'NICKNAME']: # find a name
            value = card.get(tag)
            nametag = ''.join(value).strip() if type(value) == list else value
            if nametag: break
        else: # get the next tag for a non-id
            self.uncount += 1
            nametag = "#UNKNOWN# %02d"%self.uncount

        # add the contact and nametag to self
        self[nametag] = card
